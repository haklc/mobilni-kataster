# Mobilni kataster

Repozitorij vsebuje datoteke za ogled parcelnih mej brez povezave. Več informacij najdete v objavi [Mobilna aplikacija za ogled parcelnih mej brez povezave](https://bavcar.si/sl/posts/aplikacija-za-ogled-parcelnih-mej/).


# Licence

Katastrski podatki so bili pridobljeni na spletni strani [e-Geodetski podatki](https://egp.gu.gov.si/egp/) dne 26. 10. 2023, imajo status informacij javnega značaja in so na voljo pod pogoji mednarodne licence Creative Commons 4.0. (priznanje avtorstva). Licenca je dosegljiva na spletnem naslovu https://creativecommons.org/licenses/by/4.0/deed.sl.

OpenStreetMap® so odprti podatki, ki jih je pod [Licenco za odprte podatkovne zbirke Open Data Commons](https://opendatacommons.org/licenses/odbl/) (ODbL) objavila [Fundacija OpenStreetMap](https://osmfoundation.org/) (OSMF). Podatke lahko prosto kopirate, razširjate, prenašate in prilagajate, pod pogojem, da kot vir navedete OpenStreetMap in sodelavce. Če podatke spremenite ali nadgradite, jih lahko razširjate le pod enakimi licenčnimi pogoji. Vaše pravice in obveznosti opisuje pravno besedilo. Dokumentacija je objavljena pod licenco [Creative Commons Priznanje avtorstva-Deljenje pod enakimi pogoji 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.sl) (CC BY-SA 2.0).
